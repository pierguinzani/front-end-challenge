# Front-end Challenge

Este projeto consiste no client do Sistema de Atendimento ao Cliente (SAC) do Mateus.

## :rocket: Recursos utilizados no desenvolvimento:

- React.js
- Boostrap
- HTML
- CSS
- Axios
- Hooks
- PostMan (para testar a API manualmente);
- JSON data (para retornar os dados);


## 🤔 Como rodar o projeto?

### :construction: **Pre-Requisitos** 

Antes de instalar as dependências no projeto, você precisa já ter instalado na sua máquina:


* **Node.Js**: Caso não tenha, basta realizar o download da última versão [Aqui](https://nodejs.org/en/)
* **NPM ou Yarn**: Caso também não tenha, basta realizar o download da última versão [Aqui](https://www.npmjs.com)

Após certificar-se de que tem os requisitos necessários para rodar a aplicação:

- Clone este repositório com
```shell
git clone https://gitlab.com/pierguinzani/front-end-challenge
```
- entre na pasta do projeto 
```shell
cd front-end-challenge
```
*p.s.: Se não tiver o git instalado em sua máquina, basta baixar o zip do projeto [Aqui](https://gitlab.com/pierguinzani/front-end-challenge/-/archive/master/front-end-challenge-master.zip), descompactar a pasta e prosseguir.*



###  :wrench: **Instalando as Dependências**

Depois, quando estiver na pasta do projeto, basta digitar no terminal a seguinte instrução:

```
npm install
```

Ao digitar a instrução acima, automaticamente ele irá baixar todas as dependências listadas no arquivo package.json:

* `node_modules` - que contêm os packages do npm que precisará para o projeto.


### :cyclone: Rodando a aplicação

Bom, agora na mesma tela do terminal, basta iniciar o projeto localmente:

**para rodar em ambiente de produção**
```
npm start
```

p.s.: Lembre-se de rodar a API antes. Caso não tenha baixado é só clicar [Aqui](https://gitlab.com/pierguinzani/back-end-challenge) e seguir todos os passos para rodar o backend da aplicação. 

Após iniciar o client, a aplicação estará disponível no endereço  `http://localhost:3001/`.
  

### :rotating_light: Testando a aplicação

Para testar a aplicação, basta abrir o seu navegador e entrar no endereço `http://localhost:3001/. 

### Rotas

 - `http://localhost:3001/` é a rota de signup, onde o cliente vai inserir os dados para cadastrar as informações.7

 - `http://localhost:3001/customers` é a rota onde o atendente vai olhar todos os atendimentos pendentes.

 - `http://localhost:3001/customer` é a rota onde o atendente vai visualizar os dados de um chamado e iniciar o atendimento 