import React, { useEffect, useState } from 'react';
import "./Customers.css";
import { Container, Row, Col } from 'react-bootstrap';
import api from "../../services/api"

export default () => {
  const [conversations, setConversations] = useState([]);

  useEffect(() => {
    api.get('/conversation/pending', {
      headers: {}
    }).then(response => {

      setConversations(response.data)

    })
  }, [conversations]);


  return (
    <div className="background">
      <Container id="table-container" >
        <Row>
          <Col>
            <div className="conversasContainer">
              <div className="TitleContainer">
                <div className="TitleElement">
                  <svg id="conversations-icon-header" width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24.5556 18.0556C22.75 18.0556 21.0167 17.7667 19.3989 17.2322C18.8933 17.0733 18.33 17.1889 17.9256 17.5789L14.7478 20.7567C10.66 18.6767 7.30889 15.34 5.22889 11.2522L8.40667 8.06C8.81111 7.67 8.92667 7.10667 8.76778 6.60111C8.23333 4.98333 7.94444 3.25 7.94444 1.44444C7.94444 0.65 7.29444 0 6.5 0H1.44444C0.65 0 0 0.65 0 1.44444C0 15.0078 10.9922 26 24.5556 26C25.35 26 26 25.35 26 24.5556V19.5C26 18.7056 25.35 18.0556 24.5556 18.0556ZM13 0V14.4444L17.3333 10.1111H26V0H13Z" fill="black" />
                  </svg>
                  <p className="Title">CONVERSAS</p>
                </div>
                <hr className="Titleline" />
                <p className="TitleCounter">{(!conversations.length) ? "" : conversations.length} ATENDIMENTOS PENDENTES</p>
              </div>

              <div className="ContentContainer">
                <div className="SearchContainer">
                  <i className="SearchIcon fas fa-search"></i>
                  <input
                    type="text"
                    className="SearchInput"
                    id="search"
                    placeholder="BUSCAR"
                  />
                </div>

                <div className="TableContainer">
                  <table >
            
                    <tbody>
                      {conversations.map(conversations => (
                        <tr key={conversations._id}>
                          <td>{conversations.name}</td>
                          <td><b>{conversations.state}</b></td>
                          <td><strong>{conversations.subject}</strong></td>
                          <td
                          >{(conversations.createdAt.substring(0,10) === '2020-08-04' ? 
                          conversations.createdAt.substring(11,16) : (conversations.createdAt.substring(0,10) === '2020-08-05' ? 
                          'ONTEM' : conversations.createdAt.substring(8,10) +'/' + conversations.createdAt.substring(5,7) +'/' + conversations.createdAt.substring(0,2)  ))}</td>
                        </tr>
                      ))}
                    </tbody>
                    
                  </table>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
