import React from 'react';
import useForm from "../../hooks/useForm";
import './Signup.css';
import { Container, Row, Col, Button, Dropdown } from 'react-bootstrap';
import api from "../../services/api"

export default () => {
	const [{ values, loading }, handleChange, handleSubmit] = useForm();
	const sendData = async () => {
		try {
			const response = await api.post('/conversation', values)
			alert(response.data.message);
			window.location.reload();
		} catch (err) {
			alert('Erro no cadastro das informações, tente novamente.');
		}
	};

	const handleDropItem = (eventkey, event) => {
		document.getElementById('dropdown-basic').innerHTML = event.target.text
		handleChange(event)
	};

	return (
		<div className="App">
			<Container className="d-flex justify-content-center formFrame" fluid>
				<Row>
					<Col className="colForm" xl={12} lg={12} md={12} sm={12} xs={12}>
						<form className="registerForm" onSubmit={handleSubmit(sendData)}>
							<p className="d-flex justify-content-center registerText">CADASTRE SUAS INFORMAÇÕES <br /> PARA INICIAR O ATENDIMENTO</p>

							<div className="input-container mr-3">

								<svg id="icon-signup" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 8C10.21 8 12 6.21 12 4C12 1.79 10.21 0 8 0C5.79 0 4 1.79 4 4C4 6.21 5.79 8 8 8ZM8 10C5.33 10 0 11.34 0 14V16H16V14C16 11.34 10.67 10 8 10Z" fill="black" />
								</svg>
								<input className="input-field" type="text" placeholder="NOME" name="name" onChange={handleChange} />
							</div>

							<div className="input-container">
								<svg id="icon-signup" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M0 2V16C0 17.1 0.89 18 2 18H16C17.1 18 18 17.1 18 16V2C18 0.9 17.1 0 16 0H2C0.89 0 0 0.9 0 2ZM12 6C12 7.66 10.66 9 9 9C7.34 9 6 7.66 6 6C6 4.34 7.34 3 9 3C10.66 3 12 4.34 12 6ZM3 14C3 12 7 10.9 9 10.9C11 10.9 15 12 15 14V15H3V14Z" fill="black" />
								</svg>
								<input className="input-field" type="text" placeholder="CPF" name="cpf" onChange={handleChange} />
							</div>

							<div className="input-container">
								<svg id="icon-signup" width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M14.4 0H1.6C0.72 0 0.00799999 0.72 0.00799999 1.6L0 11.2C0 12.08 0.72 12.8 1.6 12.8H14.4C15.28 12.8 16 12.08 16 11.2V1.6C16 0.72 15.28 0 14.4 0ZM14.4 3.2L8 7.2L1.6 3.2V1.6L8 5.6L14.4 1.6V3.2Z" fill="black" />
								</svg>
								<input className="input-field" type="text" placeholder="EMAIL" name="email" onChange={handleChange} />
							</div>

							<div className="input-container">
								<svg id="icon-signup" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M17.01 12.38C15.78 12.38 14.59 12.18 13.48 11.82C13.13 11.7 12.74 11.79 12.47 12.06L10.9 14.03C8.07 12.68 5.42 10.13 4.01 7.2L5.96 5.54C6.23 5.26 6.31 4.87 6.2 4.52C5.83 3.41 5.64 2.22 5.64 0.99C5.64 0.45 5.19 0 4.65 0H1.19C0.65 0 0 0.24 0 0.99C0 10.28 7.73 18 17.01 18C17.72 18 18 17.37 18 16.82V13.37C18 12.83 17.55 12.38 17.01 12.38Z" fill="black" />
								</svg>

								<input className="input-field" type="text" placeholder="TELEFONE" name="phone" onChange={handleChange} />
							</div>

							<div className="input-container">
								<svg id="icon-signup" width="13" height="18" viewBox="0 0 13 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6.3 0C2.817 0 0 2.817 0 6.3C0 11.025 6.3 18 6.3 18C6.3 18 12.6 11.025 12.6 6.3C12.6 2.817 9.783 0 6.3 0ZM6.3 8.55C5.058 8.55 4.05 7.542 4.05 6.3C4.05 5.058 5.058 4.05 6.3 4.05C7.542 4.05 8.55 5.058 8.55 6.3C8.55 7.542 7.542 8.55 6.3 8.55Z" fill="black" />
								</svg>
								<input className="input-field" type="text" placeholder="ESTADO" name="state" onChange={handleChange} />
							</div>


							<Dropdown id="show-dropdown">
								<svg id="svg-drop" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M17.1 3.6H15.3V11.7H3.6V13.5C3.6 13.995 4.005 14.4 4.5 14.4H14.4L18 18V4.5C18 4.005 17.595 3.6 17.1 3.6ZM13.5 9V0.9C13.5 0.405 13.095 0 12.6 0H0.9C0.405 0 0 0.405 0 0.9V13.5L3.6 9.9H12.6C13.095 9.9 13.5 9.495 13.5 9Z" fill="black" />
								</svg>
								<Dropdown.Toggle variant="success" id="dropdown-basic">

									ASSUNTO
									<i id="icon-drop" className="fas fa-angle-down"></i>
								</Dropdown.Toggle>
								<Dropdown.Menu id="dropDownMenu">
									<Dropdown.Item name="subject" onSelect={handleDropItem}>ELOGIO</Dropdown.Item>
									<Dropdown.Item name="subject" onSelect={handleDropItem}>RECLAMAÇÃO</Dropdown.Item>
									<Dropdown.Item name="subject" onSelect={handleDropItem}>SUGESTÃO</Dropdown.Item>
									<Dropdown.Item name="subject" onSelect={handleDropItem}>STATUS DO PEDIDO</Dropdown.Item>
									<Dropdown.Item name="subject" onSelect={handleDropItem}>CANCELAMENTO DO PEDIDO</Dropdown.Item>
									<Dropdown.Item name="subject" onSelect={handleDropItem}>ESQUECI A SENHA</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
							<Button size="lg" variant="info" type="submit" className="btn">
								<svg className="svgButton" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M17 12.5C15.75 12.5 14.55 12.3 13.43 11.93C13.08 11.82 12.69 11.9 12.41 12.17L10.21 14.37C7.38 12.93 5.06 10.62 3.62 7.79L5.82 5.58C6.1 5.31 6.18 4.92 6.07 4.57C5.7 3.45 5.5 2.25 5.5 1C5.5 0.45 5.05 0 4.5 0H1C0.45 0 0 0.45 0 1C0 10.39 7.61 18 17 18C17.55 18 18 17.55 18 17V13.5C18 12.95 17.55 12.5 17 12.5ZM9 0V10L12 7H18V0H9Z" fill="white" />
								</svg>
								{loading ? "INICIANDO..." : "INICIAR"}
							</Button>
						</form>
					</Col>
				</Row>
			</Container>
		</div>
	);
}
