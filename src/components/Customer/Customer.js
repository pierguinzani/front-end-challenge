import React, { useEffect, useState } from 'react';
import useForm from "../../hooks/useForm";
import './Customer.css'
import { Container, Row, Col, Jumbotron, Button } from 'react-bootstrap';
import api from "../../services/api"
import { useHistory } from "react-router-dom";


export default () => {
	let history = useHistory();
	const [conversation, setConversation] = useState([]);
	const [lastConversation, setLastConversation] = useState([]);
	const [{ values, loading }, handleChange, handleSubmit] = useForm();

	const meetCustomer = async () => {
		try {
			const response = await api.put('/conversation/5f29fc939c3ec350c40fd1bd', { "attended": "true" })
			alert(response.data.message);
			//window.location.reload();
			history.push("/customer");
		} catch (err) {
			alert('Erro no atendimento do cliente, tente novamente.');
		}
	};

	useEffect(() => {
		api.get('conversation/5f29fc939c3ec350c40fd1bd', {
			headers: {}
		}).then(response => {
			setConversation(response.data)
		})

		api.get('/conversation/lastconversation/12345678910', {
			headers: {}
		}).then(response => {
			setLastConversation(response.data)

		})
	}, []);


	return (

		<div className="App">
			{console.log(conversation)}
			<Container className="customerFrame" fluid>
				<Row className="rowCustomer">
					<Col className="customerInfo" xl={9} lg={9} md={12} sm={12} xs={12}>
						<Jumbotron>
							<Row className="customerRowInfo">
								<svg id="user-header" width="28" height="28" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6 6C7.6575 6 9 4.6575 9 3C9 1.3425 7.6575 0 6 0C4.3425 0 3 1.3425 3 3C3 4.6575 4.3425 6 6 6ZM6 7.5C3.9975 7.5 0 8.505 0 10.5V12H12V10.5C12 8.505 8.0025 7.5 6 7.5Z" fill="black" />
								</svg>
								<p id="customerName">CLIENTE #{(!conversation.cpf) ? " " : conversation.cpf}</p>
								<div className="rowInfo"></div>
								<div id="info-time">
									<i id="i-header" className="far fa-clock"></i>
									<p id="time">{(!conversation.createdAt) ? 'carregando...' : conversation.createdAt.substring(11, 16)}</p>
								</div>
							</Row>
							<form className="registerForm-customer" onSubmit={handleSubmit(meetCustomer)}>
								<Row>
									<Col xl={5} lg={5} md={12} sm={12} xs={12} id="col-1">
										<div className="input-container-customer">
											<label className="field">NOME:</label>
											<div className="customerField">
												<svg id="icon-customer" width="13" height="13" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M6 6C7.6575 6 9 4.6575 9 3C9 1.3425 7.6575 0 6 0C4.3425 0 3 1.3425 3 3C3 4.6575 4.3425 6 6 6ZM6 7.5C3.9975 7.5 0 8.505 0 10.5V12H12V10.5C12 8.505 8.0025 7.5 6 7.5Z" fill="black" />
												</svg>
												<input value={conversation.name} className="input-field-customer" type="text" name="name" disabled />
											</div>
										</div>

										<div className="input-container-customer">
											<label className="field">CPF:</label>
											<div className="customerField">
												<svg id="icon-customer" width="13" height="13" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0 1.33333V10.6667C0 11.4 0.593333 12 1.33333 12H10.6667C11.4 12 12 11.4 12 10.6667V1.33333C12 0.6 11.4 0 10.6667 0H1.33333C0.593333 0 0 0.6 0 1.33333ZM8 4C8 5.10667 7.10667 6 6 6C4.89333 6 4 5.10667 4 4C4 2.89333 4.89333 2 6 2C7.10667 2 8 2.89333 8 4ZM2 9.33333C2 8 4.66667 7.26667 6 7.26667C7.33333 7.26667 10 8 10 9.33333V10H2V9.33333Z" fill="black" />
												</svg>
												<input value={conversation.cpf} className="input-field-customer" type="text" name="cpf" disabled />
											</div>
										</div>

										<div className="input-container-customer">
											<label className="field">EMAIL:</label>
											<div className="customerField">
												<svg id="icon-customer" width="13" height="12" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M10.8 0.5H1.2C0.54 0.5 0.00599999 1.04 0.00599999 1.7L0 8.9C0 9.56 0.54 10.1 1.2 10.1H10.8C11.46 10.1 12 9.56 12 8.9V1.7C12 1.04 11.46 0.5 10.8 0.5ZM10.8 2.9L6 5.9L1.2 2.9V1.7L6 4.7L10.8 1.7V2.9Z" fill="black" />
												</svg>
												<input value={conversation.email} className="input-field-customer" type="text" name="email" disabled />
											</div>
										</div>

										<div className="input-container-customer">
											<label className="field">TELEFONE:</label>
											<div className="customerField">
												<svg id="icon-customer" width="13" height="13" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M11.34 8.25333C10.52 8.25333 9.72667 8.12 8.98667 7.88C8.75333 7.8 8.49333 7.86 8.31333 8.04L7.26667 9.35333C5.38 8.45333 3.61333 6.75333 2.67333 4.8L3.97333 3.69333C4.15333 3.50667 4.20667 3.24667 4.13333 3.01333C3.88667 2.27333 3.76 1.48 3.76 0.66C3.76 0.3 3.46 0 3.1 0H0.793333C0.433333 0 0 0.16 0 0.66C0 6.85333 5.15333 12 11.34 12C11.8133 12 12 11.58 12 11.2133V8.91333C12 8.55333 11.7 8.25333 11.34 8.25333Z" fill="black" />
												</svg>
												<input value={conversation.phone} className="input-field-customer" type="text" name="phone" disabled />
											</div>
										</div>

									</Col>
									<Col xl={{ span: 5, offset: 1 }} lg={{ span: 5, offset: 1 }} md={12} sm={12} xs={12} id="col-2">
										<div className="input-container-customer">
											<label className="field">ESTADO:</label>
											<div className="customerField">
												<svg id="icon-customer" width="11" height="15" viewBox="0 0 10 14" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M5.225 0.25C2.61275 0.25 0.5 2.36275 0.5 4.975C0.5 8.51875 5.225 13.75 5.225 13.75C5.225 13.75 9.95 8.51875 9.95 4.975C9.95 2.36275 7.83725 0.25 5.225 0.25ZM5.225 6.6625C4.2935 6.6625 3.5375 5.9065 3.5375 4.975C3.5375 4.0435 4.2935 3.2875 5.225 3.2875C6.1565 3.2875 6.9125 4.0435 6.9125 4.975C6.9125 5.9065 6.1565 6.6625 5.225 6.6625Z" fill="black" />
												</svg>
												<input value={conversation.state} className="input-field-customer" type="text" name="state" disabled />
											</div>
										</div>
										<div className="input-container-customer">
											<label className="field">ASSUNTO:</label>
											<div className="customerField">
												<svg id="icon-customer" width="15" height="15" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M17.1 3.6H15.3V11.7H3.6V13.5C3.6 13.995 4.005 14.4 4.5 14.4H14.4L18 18V4.5C18 4.005 17.595 3.6 17.1 3.6ZM13.5 9V0.9C13.5 0.405 13.095 0 12.6 0H0.9C0.405 0 0 0.405 0 0.9V13.5L3.6 9.9H12.6C13.095 9.9 13.5 9.495 13.5 9Z" fill="black" />
												</svg>
												<input value={conversation.subject} className="input-field-customer" type="text" name="subject" disabled />
											</div>
										</div>
										<div className="input-container-customer">
											<label className="field">ÚLTIMO PEDIDO:</label>
											<div className="customerField">
												<svg id="icon-customer" width="15" height="15" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M4.66663 11C3.93329 11 3.33996 11.6 3.33996 12.3333C3.33996 13.0667 3.93329 13.6667 4.66663 13.6667C5.39996 13.6667 5.99996 13.0667 5.99996 12.3333C5.99996 11.6 5.39996 11 4.66663 11ZM0.666626 0.333334V1.66667H1.99996L4.39996 6.72667L3.49996 8.36C3.39329 8.54667 3.33329 8.76667 3.33329 9C3.33329 9.73333 3.93329 10.3333 4.66663 10.3333H12.6666V9H4.94663C4.85329 9 4.77996 8.92667 4.77996 8.83333L4.79996 8.75333L5.39996 7.66667H10.3666C10.8666 7.66667 11.3066 7.39333 11.5333 6.98L13.92 2.65333C13.9733 2.56 14 2.44667 14 2.33333C14 1.96667 13.7 1.66667 13.3333 1.66667H3.47329L2.84663 0.333334H0.666626ZM11.3333 11C10.6 11 10.0066 11.6 10.0066 12.3333C10.0066 13.0667 10.6 13.6667 11.3333 13.6667C12.0666 13.6667 12.6666 13.0667 12.6666 12.3333C12.6666 11.6 12.0666 11 11.3333 11Z" fill="black" />
												</svg>
												<input value={(lastConversation) ? lastConversation._id : "--"} className="input-field-customer" type="text" disabled />
											</div>
										</div>
									</Col>
									<Button id="btn-submit" size="lg" variant="info" type="submit" className="btn">
										<svg className="svgButton" width="19" height="19" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M17 12.5C15.75 12.5 14.55 12.3 13.43 11.93C13.08 11.82 12.69 11.9 12.41 12.17L10.21 14.37C7.38 12.93 5.06 10.62 3.62 7.79L5.82 5.58C6.1 5.31 6.18 4.92 6.07 4.57C5.7 3.45 5.5 2.25 5.5 1C5.5 0.45 5.05 0 4.5 0H1C0.45 0 0 0.45 0 1C0 10.39 7.61 18 17 18C17.55 18 18 17.55 18 17V13.5C18 12.95 17.55 12.5 17 12.5ZM9 0V10L12 7H18V0H9Z" fill="white" />
										</svg>
										{loading ? "INICIANDO..." : "INICIAR ATENDIMENTO"}
									</Button>
								</Row>
							</form>
						</Jumbotron>
					</Col>
				</Row>
			</Container>
		</div>
	);
}
