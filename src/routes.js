import React from 'react';
import Customer from './components/Customer/Customer';
import Signup from './components/Signup/Signup';
import Customers from './components/Customers/Customers'
import { BrowserRouter, Route, Switch } from 'react-router-dom';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Signup} />
        <Route path="/customer" exact component={Customer} />
        <Route path="/customers" exact component={Customers} />
      </Switch>
    </BrowserRouter>
  );
}